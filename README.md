# Simple Scheduling Queue with RabbitMQ and AjendaJS

## Prerequisite
    RabbitMQ server - locally installed and running.

## Setup
1. Clone or Dowmload.
2. Then Run *npm install* on CLI.
3. Replace RabbitMQ connection string according to your local RabbitMQ server on *send.js* and *recieve.js* files.
4. Replace MongoDB connection string according to your local MongoDB connection on *recieve.js*.
5. The current RabbitMQ queue is *test-queue* settings are *{ durable: true, arguments: { "x-queue-type": "classic" } }*