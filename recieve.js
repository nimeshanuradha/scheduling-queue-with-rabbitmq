
var amqp = require('amqplib/callback_api');

const Agenda = require("agenda");


amqp.connect('amqp://guest:123@localhost:5672', function (error0, connection) {

    try {

        if (error0) {
            throw error0;
        }
        connection.createChannel(function (error1, channel) {
            if (error1) {
                throw error1;
            }

            var queue = 'test-queue';

            channel.assertQueue(queue, {
                durable: true,
                arguments: { "x-queue-type": "classic" }
            });

            console.log(" [*] Waiting for messages in %s. To exit press CTRL+C", queue);

            channel.consume(queue, function (msg) {
                // console.log(" [x] Received %s", msg.content.toString());
                var data_json = JSON.parse(msg.content.toString())
                console.log(data_json);
                scheduleReminderMail(data_json)

            }, {
                noAck: true
            });
        });

    } catch (error0) {
        console.log(error0);
    }

});

function scheduleReminderMail(data_json) {
    const agenda = new Agenda();

    agenda.database('localhost:27017/efeed_notifications', 'agendaJobs');

    agenda.define('SendReminderMail', async (job) => {

        console.log("Sending scheduled reminder mail....");

        // Your code for send mail

    });

    (async function () {
        try {
            await agenda.start();
            console.log("Scheduling send reminder mail task in 10 seconds");
            await agenda.schedule('in 10 seconds', 'SendReminderMail', { to: "tomail@asd.com", from: "fromail@asd.com", content: "This is the mail body of reminder mail" });
        } catch (error) {
            console.log(error);
        }

    })(data_json);
}